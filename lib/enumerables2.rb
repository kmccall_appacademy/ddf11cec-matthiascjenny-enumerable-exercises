require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  if arr.empty?
    return 0
  else
    arr.reduce(:+)
  end
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |string| sub_string?(string, substring)}
end

def sub_string?(str, substr)
  if str.length >= substr.length
    idx = 0
    while idx + substr.length <= str.length
      if str[idx, substr.length] == substr
        return true
      else
        idx += 1
      end
    end
    return false
  else
    return false
  end
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  letters = []
  array = string.split(" ").join("").split("")
  array.each do |ch|
    if array.count(ch) > 1
      array = array - [ch]
      letters << ch
    end
  end
  letters.sort
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  result = []
  array = string.split(" ")
  longest = longest_word(array)
  result << longest
  array -= [longest]
  result << longest_word(array)
  return result
end

def longest_word(array)
  array.reduce do |longest, word|
    if word.length > longest.length
      word
    else
      longest
    end
  end
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  result = []
  ("a".."z").each do |ch|
    if !(string.split("").include?(ch))
      result.push(ch)
    end
  end
  return result
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select {|n| not_repeat_year?(n)}
end

def not_repeat_year?(year)
  year.to_s.each_char do |ch|
    if year.to_s.count(ch) > 1
      return false
    end
  end
  return true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  result = songs.select { |song_name| no_repeats?(song_name, songs) }
  result.uniq
end

def no_repeats?(song_name, songs)
  i = 0
  while i + 1 < songs.length
    if songs[i] == songs[i + 1] && songs[i + 1] == song_name
      return false
    else
      i += 1
    end
  end
  return true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  arr = remove_punctuation(string).split(" ")
  if arr.all? { |word| c_distance(word) == -1 }
    return ""
  else
    arr.reduce do |closest, word|
      if c_distance(word) == -1
        closest
      elsif c_distance(closest) == -1
        word
      elsif c_distance(word) > -1 && c_distance(word) > -1\
          && c_distance(closest) <= c_distance(word)
          closest
      else
        word
      end
    end
  end
end

def c_distance(word)
  i = -1
  while i.abs <= word.length
    if word[i] == "c"
      return i.abs - 1
    else
      i -= 1
    end
  end
  return -1
end

def remove_punctuation(string)
  punctuation = [",",".","!","?",":",";"]
  arr = string.split(" ")
  arr.each_with_index do |word,i|
    array_pruned = word.split("").reject do |ch|
      punctuation.include?(ch)
    end
    arr[i] = array_pruned.join("")
  end
  return arr.join(" ")
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  i = 0
  j = 0
  result = []
  while j < arr.length
    while arr[i] == arr[j+1]
      j += 1
    end
    if i != j
      result.push([i,j])
    end
    i = j + 1
    j = i
  end
  return result
end
